require('dotenv').config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();
const request = require('request-json');
const port = process.env.PORT || 3000;
const usersFile = require('./user.json');
const URL_BASE = '/techu/v2';
const URL_MLAB = 'https://api.mlab.com/api/1/databases/techu50db/collections/';
const APIKEY = 'apiKey=' + process.env.APIKEYMLAB;

app.listen(port, () => {
    console.log('Node JS escuchando', port);
});

app.use(body_parser.json());

/**********************/
/****** SEMANA 3 *****/
app.get(URL_BASE + '/users', (req, res) => {
  const httpClient = request.createClient(URL_MLAB);
  const fieldParameter = 'f={"_id": 0}&';
  httpClient.get('user?' + fieldParameter + APIKEY, (err, respuestaMLab, body) => {
    var response = {};
    if(err) {
        response = { msg : "Error al recuperar users de mLab." };
        res.status(500);
    } else {
      if(body.length > 0) {
        response = body;
      } else {
        response = { msg : "Usuario no encontrado." };
        res.status(204);
      }
    }
    res.status(200).send(response);
  });
});

app.get(URL_BASE + '/users/:id', (req, res) => {
  let id = req.params.id;
  const httpClient = request.createClient(URL_MLAB);
  const queryString = `q={"id_user": ${id}}&`;
  const fieldString = 'f={"_id": 0}&';
  httpClient.get('user?' + queryString + fieldString + APIKEY, (err, respuestaMLab, body) => {
    let response = {};
    if(err) {
      response = { msg : "Error al recuperar user de mLab." };
      res.status(500);
    }
    if(body.length > 0) {
      response = body;
    } else {
      response = { msg : "Usuario no encontrado." };
      res.status(204);
    }
    res.status(200).send(response);
  });
});

app.get(URL_BASE + '/users/:id/accounts', (req, res) => {
  let id = req.params.id;
  const httpClient = request.createClient(URL_MLAB);
  const queryString = `q={"id_user": ${id}}&`;
  const fieldString = 'f={"_id": 0}&';
  httpClient.get('user?' + queryString + fieldString + APIKEY, (err, respuestaMLab, data) => {
    let response = {};
    if(err) {
      response = { msg : "Error al recuperar user de mLab." };
      res.status(500);
    }
    if(data.length > 0) {
      response = data[0].account;
    } else {
      response = { msg : "Usuario no encontrado." };
      res.status(204);
    }
    res.status(200).send(response);
  });
});

//POST of user
app.post(URL_BASE + '/users', (req, res) => {
  const httpClient = request.createClient(URL_MLAB);
  httpClient.get('user?'+ APIKEY , (error, respuestaMLab , body) => {
      newID = body.length + 1;
      let newUser = {
        id_user : newID,
        first_name : req.body.first_name,
        last_name : req.body.last_name,
        email : req.body.email,
        password : req.body.password
      };
      httpClient.post('user?' + APIKEY, newUser , (error, respuestaMLab, body) => {
        res.send(body);
     });
  });
});

app.put(URL_BASE + '/users/:id', (req, res) => {
  const { id } = req.params;
  const queryString = `q={"id_user": ${id}}&`;
  const httpClient = request.createClient(URL_MLAB);
  httpClient.get(`user?${queryString}${APIKEY}`, (error, respuestaMLab , body) => {
    let cambio = `{"$set": ${JSON.stringify(req.body)}}`;
    httpClient.put(`user?${queryString}${APIKEY}`, JSON.parse(cambio), (error, respuestaMLab, body) => {
      console.log("body:"+ body);
      res.send(body);
    });
  });
});

//DELETE user with id
app.delete(URL_BASE + 'users/:id', (req, res) => {
  console.log('entra al DELETE');
  console.log('request.params.id: ' + req.params.id);
  let id = req.params.id;
  let queryStringID = 'q={"id":' + id + '}&';
  console.log(baseMLabURL + 'user?' + queryStringID + APIKEY);
  const httpClient = request.createClient(URL_MLAB);
  httpClient.get('user?' +  queryStringID + APIKEY, (error, respuestaMLab, body) => {
    var respuesta = body[0];
    console.log('body delete:'+ JSON.stringify(respuesta));
    httpClient.delete('user/' + respuesta._id.$oid +'?'+ APIKEY, (error, respuestaMLab,body) => {
        res.send(body);
    });
  });
});

//Method POST login
app.post(URL_BASE + '/login', (req, res) => {
  console.log('POST /colapi/v3/login');
  const { email, password } = req.body;
  const queryString = 'q={"email":"' + email + '","password":"' + password + '"}&';
  const limFilter = 'l=1&';
  const httpClient = request.createClient(URL_MLAB);
  httpClient.get('user?'+ queryString + limFilter + APIKEY, (error, respuestaMLab, body) => {
    if (!error) {
      if (body.length == 1) { // Existe un usuario que cumple 'queryString'
        let login = '{"$set":{"logged":true}}';
        httpClient.put('user?q={"id_user": ' + body[0].id_user + '}&' + APIKEY, JSON.parse(login), (errPut, resPut, bodyPut) => {
          res.send({ msg: 'Login correcto', 'user':body[0].email, 'userid':body[0].id, 'name':body[0].first_name});
        });
      }
      else {
        res.status(404).send({ msg: 'Usuario no válido.'});
      }
    } else {
      res.status(500).send({ msg: 'Error en petición a mLab.'});
    }
  });
});

//Method POST logout
app.post(URL_BASE + '/logout', (req, res) => {
  console.log('POST /colapi/v3/logout');
  var email= req.body.email;
  var queryStringEmail='q={"email":"' + email + '"}&';
  const httpClient = request.createClient(URL_MLAB);
  httpClient.get('user?'+ queryStringEmail + APIKEY , (error, respuestaMLab , body) => {
    console.log('entro al get');
    var respuesta = body[0];
    console.log(respuesta);
    if (respuesta != undefined) {
      console.log('logout Correcto');
      var session = { "logged": true };
      var logout = '{"$unset":' + JSON.stringify(session) + '}';
      console.log(logout);
      httpClient.put('user?q={"id_user": ' + respuesta.id_user + '}&' + APIKEY, JSON.parse(logout), (errorP, respuestaMLabP, bodyP) => {
        res.send(body[0]);
      });
    } else {
      console.log('Error en logout');
      res.send({ msg: 'Error en logout'});
    }
  });
});

/**********************/
/****** SEMANA 2 *****/
app.get(URL_BASE + '/users1', (req, res) => {
    res.status(200).send(usersFile);
});

app.get(URL_BASE + '/users1/:id', (req, res) => {
    let id = req.params.id;
    let user = usersFile.find(user => user.id === id);
    if (user) {
      res.status(200).send({ user });
    } else {
      res.status(404).send({ mensaje: 'Usuario no existente' });
    }
});

app.post(URL_BASE + '/users1', (req, res) => {
    const user = { id: usersFile.length, first_name: 'Kimble', last_name: 'Robuchon', email: 'krobuchon0@phpbb.com', password: 'RIKsYgf44xJK' };
    usersFile.push(user);
    res.send(user);
});

app.put(URL_BASE + '/users1/:id', (req, res) => {
    let userUpdated = usersFile[req.params.id - 1];
    if (userUpdated) {
      userUpdated.first_name = req.body.first_name;
      userUpdated.last_name = req.body.last_name;
      userUpdated.email = req.body.email;
      userUpdated.password = req.body.password;
      res.status(200).send({ mensaje: 'Usuario actualizado correctamente', userUpdated });
    } else {
      res.send({ mensaje: 'Usuario no encontrado' });
    }
});

// Petición GET con Query String (req.query)
app.get(URL_BASE + '/users3', (req, res) => {
    console.log("GET con query string.");
    console.log(req.query.id);
    console.log(req.query.country);
    res.send(usersFile[pos - 1]);
    respuesta.send({"msg" : "GET con query string"});
});

// LOGIN - user.json
app.post(URL_BASE + '/login1', (req, res) => {
    let email = req.body.email;
    let pass = req.body.password;
    let userLogged = usersFile.find(user => user.email === email && user.password === pass);
    if (userLogged) {
      userLogged.logged = true;
      writeUserDataToFile(usersFile);
      res.status(200).send({ mensaje : 'Login correcto.', idUsuario : userLogged.id, logged : 'true' });
    } else {
      res.status(401).send({ mensaje : 'Login incorrecto.'});
    }
});

// LOGOUT - user.json
app.post(URL_BASE + '/logout1', (req, res) => {
  var userId = req.body.id;
  let userLogged = usersFile.find(user => user.id === userId);
  if (userLogged && userLogged.logged) {
    delete userLogged.logged;
    writeUserDataToFile(usersFile);
    res.status(200).send({ mensaje : 'Logout correcto.', idUsuario : userLogged.id});
  } else {
    res.send({ mensaje : 'Logout incorrecto.'});
  }
});

// Devuelve la cantidad de usuarios
app.get(URL_BASE + '/users-total', (req, res) => {
  res.send({ total : usersFile.length });
});

function writeUserDataToFile(data) {
    const fs = require('fs');
    let jsonUserData = JSON.stringify(data);
    fs.writeFile("./user.json", jsonUserData, "utf8", (err) => {
      if (err) {
          console.log(err);
      } else {
          console.log("Datos escritos en 'users.json'.");
      }
    });
}