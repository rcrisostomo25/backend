# Imagen docker base inicial
FROM node:latest

# Crea directorio de trabajo
WORKDIR /docker-apitechu

# Copiar archivos del proyecto
ADD . /docker-apitechu

# instalar dependencias
# RUN npm install --only=production

# Puerto donde exponemos contenedor (mismo que usamos en api)
EXPOSE 3000

# comando para lanzar la app
CMD ["npm", "run", "prod"]

